package com.customerdetails.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.bind.annotation.RequestAttribute;

import com.customerdetails.repository.CustomerRepo;
import com.customerdetails.service.CustomerServiceimpl;

import com.customerdetails.customer.entity.*;

@SpringBootTest
public class CustomerControllerTest {
	@Mock
	private CustomerServiceimpl customerservice;
	@InjectMocks
	private CustomerController customercontroller;
	@Test
	public void loginvalid() {
		String customerName = "vaibhav";
		String password = "password";
		String Exceptedresult = "login success";
		
		Mockito.when(customerservice.login(customerName, password)).thenReturn(Exceptedresult);
		String actualResult = customercontroller.login(customerName, password);
		assertEquals(Exceptedresult,actualResult);
		
	}
	
	
	
	
     
}
