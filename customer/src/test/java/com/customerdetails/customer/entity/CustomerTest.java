package com.customerdetails.customer.entity;

	import static org.junit.Assert.assertEquals;
	import static org.junit.Assert.assertNotNull;
	import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.Test;

public class CustomerTest {
		
		@Test
		public void testConstructorAndGetters() {
			Customer customer = new Customer("vaibhav", "password123");
			
			assertEquals("vaibhav", customer.getCustomerName());
			assertEquals("password123", customer.getPassword());
			assertEquals(0, customer.getPhone_no());
			assertEquals(0, customer.getCustomer_id());
		}
		
		@Test
		public void testSetters() {
			Customer customer = new Customer();
			
			customer.setCustomerName("sai");
			customer.setPassword("password456");
			customer.setPhone_no(1234567890);
			customer.setCustomer_id(1);
			
			assertEquals("sai", customer.getCustomerName());
			assertEquals("password456", customer.getPassword());
			assertEquals(1234567890, customer.getPhone_no());
			assertEquals(1, customer.getCustomer_id());
		}
		
		@Test
		public void testDefaultConstructor() {
			Customer customer = new Customer();
			
			assertNull(customer.getCustomerName());
			assertNull(customer.getPassword());
			assertEquals(0, customer.getPhone_no());
			assertEquals(0, customer.getCustomer_id());
		}

	}



