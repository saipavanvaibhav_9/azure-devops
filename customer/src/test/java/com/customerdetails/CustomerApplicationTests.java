package com.customerdetails;

import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(SpringRunner.class)
@SpringBootTest
class CustomerApplicationTests {
	
	

	 @RepeatedTest(7)
	 @DisplayName("Context testing method.")
	    public void contextLoads() {
	        CustomerApplication app = new CustomerApplication();
	        assertNotNull(app , "new test case passed.");
	    }

}
