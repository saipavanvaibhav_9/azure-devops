package com.customerdetails.interceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class SinglepurposeInterceptor implements HandlerInterceptor{
		
		final Logger log = LoggerFactory.getLogger(SinglepurposeInterceptor.class);
		@Override
		public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
				throws Exception {
			log.info("Request URL: {}", request.getRequestURL());

			String password = request.getParameter("password");

			log.info("User Login Attempt: password={}", password);


			 
			return true;
		}
		
		@Override
		public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
				ModelAndView modelAndView) throws Exception {
			 log.info("Inside posthandle");
		}
		
		@Override
		public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
				throws Exception {
			 log.info("Inside aftercompletion");
		}

	}



