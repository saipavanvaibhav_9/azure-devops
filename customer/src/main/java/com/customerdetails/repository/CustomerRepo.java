package com.customerdetails.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.customerdetails.customer.entity.Customer;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Integer>{

    Optional<Customer> findByCustomerName(String customerName); 
    

}

